
load('/Users/hart/Documents/workspace/gs-scheduling-tasks/complete/src/main/resources/lib/jvm-npm.js');
score = require('/Users/hart/Documents/workspace/gs-scheduling-tasks/complete/src/main/resources/lib/underscore.js');

Class = Java.type("java.lang.Class");
URL = Java.type("java.net.URL");
BufferedReader = Java.type("java.io.BufferedReader");
InputStreamReader = Java.type("java.io.InputStreamReader");
JSONObject = Java.type("org.json.JSONObject");
Parser = Java.type("org.json.simple.parser.JSONParser");

var base_url = 'http://api-wpm.apicasystem.com/v3/';
var service = 'checks';
var auth_ticket = '8367E0A8-FF00-4D36-8178-419BC1366C08';
var getChecksURL = base_url + service + '?' + 'auth_ticket=' + auth_ticket;
var getGroupsURL = base_url + 'groups' + '?' + 'auth_ticket=' + auth_ticket;
var groupList = {};
var checksToGroups = {};
var finalList = [];
var urlToCheckId = {};

var ft = new java.text.SimpleDateFormat ("yyyy.MM.dd");

var index = 'apica-' + ft.format(new java.util.Date());
var type = 'check';

var groups = getResponse(getGroupsURL);
processGroups(groups);
processGroupChecks(groupList);

var checks = getResponse(getChecksURL);
processChecksBrowser(checks);
//print("checks:" + checks);
processChecks(checks,checksToGroups);



print("browserUrls:" + JSON.stringify(urlToCheckId));


sendDataToES(index,type);

print("completed run....");


function getBrowserURL(checkId,resultId){

    //resultId = "b80335e1-32c8-42b8-b810-3960da319868";
    //checkId = "71257";

    var _data = {"result_ids": [ resultId ] };
    var data = JSON.stringify(_data);
    var url = base_url + service + '/browser/' + checkId + '/results/urldata?' + 'auth_ticket=' + auth_ticket;


    try {
        var returnObject = httpPost(url, data);


        //var returnObject = httpPost(url, data);

        var myJson = JSON.parse(returnObject.data);
        //print("myJson:" + JSON.stringify(myJson));
        var newUrl = myJson['check_results'][0]['url_results'][0].url;
        return newUrl;
    } catch(err) {
        print("error:" + err);
    }

}


function processChecksBrowser(checkResults){

    for ( var i = 0; i < checkResults.length; i++) {
        var check = checkResults[i];
        var checkId = check.id;

        if (check.check_type_api === 'browser') {
            var url = base_url + service + '/' + checkId + '/results?mostrecent=1&detail_level=1&auth_ticket=' + auth_ticket;
            
            try {
                var returnObject = httpGet(url);
                var item = JSON.parse(returnObject.data);
                var checkId = item[0].check_id;
                var resultId = item[0].identifier;
                var targetURL = getBrowserURL(checkId, resultId);
                urlToCheckId[checkId] = targetURL;
            } catch(err) {
                print("error with processChecksBrowser:" + err);
            }
        }
    }
}


function processChecks(checkResults,checkList){
    for ( var i = 0; i < checkResults.length; i++) {
        var c = score._.clone(checkResults[i]);
        var checkId = c.id;
        var groupId = checkList[checkId].groupId;
        var groupName = checkList[checkId].groupName;
        var subGroupName = checkList[checkId].subgroupName;
        var subGroupId = checkList[checkId].subgroupId;

        if (c.check_type_api === 'browser') {
            print("check_type_api:" + c.check_type_api);
            var checkId = c.id;
            var targetURL = urlToCheckId[checkId];
            score._.extend(c,{targetURL: targetURL});
        }

        score._.extend(c,{groupId: groupId});
        score._.extend(c,{groupName: groupName});
        score._.extend(c,{subGroupName: subGroupName});
        score._.extend(c,{subGroupId: subGroupId});

        finalList.push(c);
    }
}

function processGroupChecks(groups){

    for(var key in groups){
        var groupId = key;
        var getChecksInGroupURL = base_url + 'groups/' + groupId + '/checks?auth_ticket=' + auth_ticket;
        var checks = getResponse(getChecksInGroupURL);

        for ( var i = 0; i < checks.length; i++){
            var c = {};
            var check = checks[i];
            var match = groups[groupId].subgroupName;
            var subGroupId = groups[groupId].subgroupId;
            checksToGroups[check] = groups[groupId]
        }
    }
}

function processGroups(groupResult){
    for ( var i = 0; i < groupResult.length; i++) {
        groupResult[i].groups.forEach(function(subgroup){
            var g = {};

            g.groupName = groupResult[i].name;
            g.groupId = groupResult[i].id;
            g.subgroupName = subgroup.name;
            g.subgroupId = subgroup.id;

            groupList[g.subgroupId] = g;
        })
    }
}

function getResponse(myUrl){
    var url = new URL(myUrl);
    var connection = url.openConnection();
    var inputStream = new InputStreamReader(connection.getInputStream());
    var reader = new BufferedReader(inputStream);
    var dataChunk = reader.readLine();
    reader.close();
    parser = new Parser();
    var data = parser.parse(dataChunk);
    return data;
}

function sendDataToES(index,type){
    var finalData = '';

    for ( i = 0; i < finalList.length; i++) {
        var guid = finalList[i].guid;
        var ts = finalList[i].timestamp_utc;
        var action = '{ "create": { "_index": "' + index + '", "_type": "' + type + '", "_id": "' + guid + ts + '" }}';

        finalData = finalData + action + '\n';
        finalData = finalData + JSON.stringify(finalList[i]) + '\n';
    }
    httpPost("http://darwin:9200/_bulk", finalData);
}

/*************
 UTILITY FUNCTIONS
 *************/

function httpGet(theUrl){
    var con = new java.net.URL(theUrl).openConnection();
    con.requestMethod = "GET";

    return asResponse(con);
}

function httpPost(theUrl, data, contentType){
    contentType = contentType || "application/json";
    var con = new java.net.URL(theUrl).openConnection();

    con.requestMethod = "POST";
    con.setRequestProperty("Content-Type", contentType);

    // Send post request
    con.doOutput=true;
    write(con.outputStream, data);

    return asResponse(con);
}

function asResponse(con){
    var d = read(con.inputStream);

    return {data : d, statusCode : con.responseCode};
}

function write(outputStream, data){
    var wr = new java.io.DataOutputStream(outputStream);
    wr.writeBytes(data);
    wr.flush();
    wr.close();
}

function read(inputStream){
    var inReader = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream));
    var inputLine;
    var response = new java.lang.StringBuffer();

    while ((inputLine = inReader.readLine()) != null) {
        response.append(inputLine);
    }
    inReader.close();
    return response.toString();
}

